# EGMZ-Product-Card

This is a package of tests in NPM

### Eric Gomez

## Example

```
import {ProductCard, ProductImage, ProductTitle, ProductButtons} from 'egmz-product-card';
```

```
const product = {
  id: '1',
  title: 'Product 1',
  //   image: 'https://via.placeholder.com/150',
};

<ProductCard
        key={product.id}
        product={product}
        initialValues={{
          count: 4,
          maxCount: 10,
        }}
      >
        {({ reset, count, isMaxCountReached, maxCount, increaseBy }) => (
          <>
            <ProductImage />
            <ProductTitle />
            <ProductButtons />
          </>
        )}
      </ProductCard>
```