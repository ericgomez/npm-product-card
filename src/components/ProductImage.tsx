import React, { CSSProperties, useContext } from 'react';

import styles from '../styles/styles.module.css';
import { ProductContext } from './ProductCard';
import noImage from '../assets/no-image.jpg';

export interface Props {
  image?: string;
  className?: string;
  style?: CSSProperties;
}

export const ProductImage = ({ image, className, style }: Props) => {
  //Utilizing the useProduct hook to get the product data
  const { product } = useContext(ProductContext);

  let imgToShow: string;

  if (image) {
    imgToShow = image;
  } else if (product.image) {
    imgToShow = product.image;
  } else {
    imgToShow = noImage;
  }

  return (
    <img
      className={`${styles.productImg} ${className}`}
      style={style}
      src={imgToShow}
      alt="Product image"
    />
  );
};
